<?php
/**
 * Copyright 2018, Eduard Minasyan <mrEDitor@mail.ru>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Tags module data initialization.
 *
 * @package smart_tags
 * @since 1.0.0
 * @author Eduard Minasyan <mrEDitor@mail.ru>
 */
class PluginSmartTags_Update_TagsTable extends ModulePluginManager_EntityUpdate
{

    /**
     * Create plugin tables.
     *
     * @return void
     */
    public function up(): void
    {
        $sTable = $this->_getTable();

        $sTopicId = PluginSmartTags_ModuleTags_MapperTag::FIELD_TOPIC_ID;
        $sTag = PluginSmartTags_ModuleTags_MapperTag::FIELD_TAG;
        $sPriority = PluginSmartTags_ModuleTags_MapperTag::FIELD_PRIORITY;

        if (!$this->isTableExists($sTable)) {
            $this->exportSQLQuery(
            /** @lang MySQL */
<<<SQL
            CREATE TABLE `{$sTable}` (
                `{$sTopicId}` INT(11) NOT NULL,
                `{$sTag}` VARCHAR(255) NOT NULL,
                `{$sPriority}` INT(11) NOT NULL,
                INDEX (`{$sTopicId}`),
                INDEX (`{$sTag}`)
            ) ENGINE = InnoDB;
SQL
            );
        }
    }

    /**
     * Drop data tables.
     *
     * @return void
     */
    public function down(): void
    {
        $sTable = $this->_getTable();
        if ($this->isTableExists($sTable)) {
            $this->exportSQLQuery("DROP TABLE `{$sTable}`;");
        }
    }

    /**
     * Get tags table name.
     *
     * @return string
     */
    protected function _getTable(): string
    {
        $aValue = include(Plugin::GetPath(__CLASS__) . 'config/config.php');
        Config::Load($aValue['$root$'], false);
        return Config::Get('db.table.smart_tags');
    }

}
