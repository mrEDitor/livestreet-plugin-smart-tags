<?php
/**
 * Copyright 2017, Eduard Minasyan <mrEDitor@mail.ru>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Smart Tags actions processor.
 *
 * @method string Lang_Get(string $sKey, array $aReplace = [], bool $bDelete = true)
 * @method void Logger_Error(string $sMsg, array $aContext = array(), string $sInstance = 'default')
 * @method string[]|null PluginSmartTags_ModuleTags_GetTagsFromText(string $text)
 * @method void Viewer_SetResponseAjax(string $sResponseAs = 'json', bool $bHeaders = true, bool $bValidate = true)
 * @method void Viewer_AssignAjax(string $key, mixed $value = null)
 *
 * @package smart_tags.actions
 * @since 1.0.0
 * @author Eduard Minasyan <mrEDitor@mail.ru>
 */
class PluginSmartTags_ActionSmartTags extends Action
{

    /**
     * Initialize action.
     *
     * @return void
     */
    public function Init(): void
    {
        $this->Viewer_SetResponseAjax('json', true, false);
    }

    /**
     * Register provided events.
     *
     * @return void
     */
    protected function RegisterEvent(): void
    {
        $this->AddEvent('process', 'EventProcess');
    }

    /**
     * Process text and associate tags with it.
     *
     * @return void
     */
    protected function EventProcess(): void
    {
        $text = getRequest('text');
        try {
            $tags = $this->PluginSmartTags_ModuleTags_GetTagsFromText($text);
            $this->Viewer_AssignAjax('status', 'success');
            $this->Viewer_AssignAjax('suggestions', array_filter($tags));
        } catch (Exception $e) {
            $this->Logger_Error($e->getMessage());
            $this->Viewer_AssignAjax('status', 'error');
            $this->Viewer_AssignAjax('message', 'Tags Extraction is not available now.');
        }
    }

}
