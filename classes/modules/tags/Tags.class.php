<?php
/**
 * Copyright 2018, Eduard Minasyan <mrEDitor@mail.ru>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Tags module main class.
 *
 * @method string Lang_GetLang()
 * @method ModuleTopic_EntityTopic[] Topic_GetTopicsByArrayId(array $aTopicIds)
 *
 * @package smart_tags.modules.tags
 * @since 1.0.0
 * @author Eduard Minasyan <mrEDitor@mail.ru>
 */
class PluginSmartTags_ModuleTags extends Module
{

    /** @var PluginSmartTags_ModuleTags_MapperTag */
    protected $oMapperTag;

    /** @var ModuleTopic_EntityTopic */
    protected $oCurrentTopic;

    /**
     * Initialize module.
     *
     * @return void
     */
    public function Init(): void
    {
        $this->oMapperTag = Engine::GetMapper(__CLASS__, 'Tag');
    }

    /**
     * Get similar topics to specified one.
     *
     * @param ModuleTopic_EntityTopic $oTopic
     * @return ModuleTopic_EntityTopic[]
     */
    public function GetSimilarTopics(ModuleTopic_EntityTopic $oTopic): array
    {
        $iTopicsLimit = Config::Get('plugin.smart_tags.similar_topics.limit');
        $aTopicIds = $this->oMapperTag->getTopicIdsByCommonTags($oTopic->getId(), $iTopicsLimit);
        $aTopics = $this->Topic_GetTopicsByArrayId($aTopicIds);
        return $aTopics;
    }

    /**
     * Save topic tags.
     *
     * @param int   $iTopicId   Topic identifier to associate tags with.
     * @param array $aTags      Suggested tags in ascending priority order.
     * @return void
     */
    public function SaveTopicTags(int $iTopicId, array $aTags): void
    {
        $this->oMapperTag->setTopicTags($iTopicId, $aTags);
    }

    /**
     * Process text to sorted in ascending priority order tags list.
     *
     * @param string $text  Text to process.
     * @return string[]
     * @throws Exception In case of tags extraction fails.
     */
    public function GetTagsFromText($text): ?array
    {
        $url = Config::Get('plugin.smart_tags.extractor_url');
        $params = [
            'lang' => $this->Lang_GetLang(),
            'text' => $text,
            'count' => Config::Get('plugin.smart_tags.suggestions.limit'),
        ];
        $jsonParams = json_encode($params);
        $options = [
            'http' => [
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                            "Content-Length: " . strlen($jsonParams) . "\r\n",
                'method' => 'POST',
                'content' => $jsonParams,
            ]
        ];

        $context = stream_context_create($options);
        if ($result = file_get_contents($url, false, $context)) {
            $result = json_decode($result, true);
            if ($result['status'] == 'success') {
                return $result['keywords'];
            }
            throw new Exception($result['message']);
        } else {
            throw new Exception('Tags Extractor service is unavailable');
        }
    }

    /**
     * Register currently showing topic.
     *
     * @param ModuleTopic_EntityTopic $oTopic Topic to show on page.
     * @return void
     */
    public function RegisterCurrentTopic(ModuleTopic_EntityTopic $oTopic): void
    {
        $this->oCurrentTopic = $oTopic;
    }

    /**
     * Get registered currently showing topic.
     *
     * @return ModuleTopic_EntityTopic
     */
    public function RegistryCurrentTopic(): ?ModuleTopic_EntityTopic
    {
        return $this->oCurrentTopic;
    }

}
