<?php
/**
 * Copyright 2018, Eduard Minasyan <mrEDitor@mail.ru>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Tag Entity mapper class.
 *
 * @package smart_tags.modules.tags
 * @since 1.0.0
 * @author Eduard Minasyan <mrEDitor@mail.ru>
 */
class PluginSmartTags_ModuleTags_MapperTag extends Mapper
{

    const ENTITY = 'PluginSmartTags_Tag';

    const FIELD_TOPIC_ID = 'topic_id';
    const FIELD_TAG = 'tag';
    const FIELD_PRIORITY = 'priority';

    const FIELDS = [
        self::FIELD_TOPIC_ID,
        self::FIELD_TAG,
        self::FIELD_PRIORITY,
    ];

    /**
     * Get identifiers of published topics with same tags as specified one.
     * Result does not contain specified topic identifier.
     *
     * @param int $iTopicId Topic identifier.
     * @param int $iLimit   Topics limit in result.
     * @return string[]
     */
    public function getTopicIdsByCommonTags(int $iTopicId, int $iLimit): array
    {
        $aRows = $this->oDb->query($this->_getSelectSimilarTopicsByTopicIdSql($iTopicId, $iLimit));
        $aTopicIds = array_column($aRows, 'topic_id');
        return array_diff($aTopicIds, [$iTopicId]);
    }

    /**
     * Save topic-tag pairs.
     *
     * @param int $iTopicId     Associated topic ID.
     * @param string[] $aTags   Suggested tags for topic in ascending priority order.
     * @return void
     */
    public function setTopicTags(int $iTopicId, array $aTags): void
    {
        $this->oDb->query($this->_getDeleteByTopicIdSql($iTopicId));
        if ($aTags = array_filter($aTags)) {
            $this->oDb->query($this->_getInsertTagsForTopicSql($iTopicId, $aTags));
        }
    }

    /**
     * Get SELECT for identifiers of published topics with same tags as specified one.
     *
     * @param int $iTopicId Topic identifier.
     * @param int $iLimit   Topics limit in result.
     * @return string
     */
    protected function _getSelectSimilarTopicsByTopicIdSql(int $iTopicId, int $iLimit): string
    {
        $sTopicId = $this->_escapeIdentifier(self::FIELD_TOPIC_ID);
        $sTag = $this->_escapeIdentifier(self::FIELD_TAG);
        $sPriority = $this->_escapeIdentifier(self::FIELD_PRIORITY);
        return /** @lang MySQL */
<<<SQL
        SELECT DISTINCT `similar`.{$sTopicId}
            FROM {$this->_getTable()} `current`
            JOIN {$this->_getTable()} `similar`
                USING ({$sTag})
            JOIN {$this->_getTopicTable()} `topics`
                ON {$this->_getTopicJoinCondition('similar', 'topics')}
                AND `topics`.`topic_publish`=1
            WHERE {$this->_getTopicIdFilter($iTopicId, 'current')}
            ORDER BY `current`.{$sPriority} + `similar`.{$sPriority}
            LIMIT {$iLimit}
SQL;
    }

    /**
     * Get DELETE query by topic identifier.
     *
     * @param int $iTopicId Topic identifier.
     * @return string
     */
    protected function _getDeleteByTopicIdSql(int $iTopicId): string
    {
        return /** @lang MySQL */
<<<SQL
        DELETE FROM {$this->_getTable()}
            WHERE {$this->_getTopicIdFilter($iTopicId)}
SQL;
    }

    /**
     * Get INSERT INTO query with topic-tag pairs.
     *
     * @param int       $iTopicId   Associated topic id.
     * @param string[]  $aTags      Sorted tags to save.
     * @return string
     */
    protected function _getInsertTagsForTopicSql(int $iTopicId, array $aTags): string
    {
        return /** @lang MySQL */
<<<SQL
        INSERT INTO {$this->_getTable()} ({$this->_getFields()})
            VALUES {$this->_getValues($iTopicId, $aTags)}
SQL;
    }

    /**
     * Get table name for entity.
     *
     * @return string
     */
    protected function _getTable(): string
    {
        return $this->_escapeIdentifier(Config::Get('db.table.smart_tags'));
    }

    /**
     * Get table name for topic entity.
     *
     * @return string
     */
    protected function _getTopicTable(): string
    {
        return $this->_escapeIdentifier(Config::Get('db.table.topic'));
    }

    /**
     * Get topic table join condition.
     *
     * @param string $sTagTableName TopicID-tag pairs table name.
     * @param string $sTopicTableName Topics table name.
     * @return string
     */
    protected function _getTopicJoinCondition($sTagTableName, $sTopicTableName): string
    {
        $sTagTableName = $this->_escapeIdentifier($sTagTableName);
        $sTopicTableName = $this->_escapeIdentifier($sTopicTableName);
        $sTopicIdField = $this->_escapeIdentifier(self::FIELD_TOPIC_ID);
        return "{$sTagTableName}.{$sTopicIdField}={$sTopicTableName}.{$sTopicIdField}";
    }

    /**
     *
     */
    protected function _getFields(): string
    {
        $fields = array_map([$this, '_escapeIdentifier'], self::FIELDS);
        return implode(',', $fields);
    }

    /**
     * Get topic id field row where condition.
     *
     * @param int       $iTopicId   Topic identifier.
     * @param string    $sTableName Main table alias.
     * @return string
     */
    protected function _getTopicIdFilter(int $iTopicId, string $sTableName = ''): string
    {
        if ($sTableName) {
            $sTableName = $this->_escapeIdentifier($sTableName) . '.';
        }
        return $sTableName . $this->_escapeIdentifier(self::FIELD_TOPIC_ID) . '=' . $iTopicId;
    }

    /**
     * Get topic-tag pairs values for inserting.
     *
     * @param int       $iTopicId   Topic identifier.
     * @param string[]  $aTags      Sorted tags values.
     * @return string
     */
    protected function _getValues(int $iTopicId, array $aTags): string
    {
        $aValues = [];
        foreach (array_values($aTags) as $iPriority => $sTag) {
            $aValuesList = [
                $iTopicId,
                $this->oDb->escape($sTag),
                $iPriority,
            ];
            $aValues[] = '(' . implode(',', $aValuesList) . ')';
        }
        return implode(',', $aValues);
    }

    /**
     * Escape SQL identifier.
     *
     * @param string $identifier    Identifier to escape.
     * @return string
     */
    protected function _escapeIdentifier(string $identifier): string
    {
        return $this->oDb->escape($identifier, true);
    }

}
