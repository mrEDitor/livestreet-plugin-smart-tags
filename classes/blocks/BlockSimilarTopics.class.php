<?php
/**
 * Copyright 2018, Eduard Minasyan <mrEDitor@mail.ru>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Smart Tags actions processor.
 *
 * @method ModuleViewer Viewer_GetLocalViewer()
 * @method ModuleTopic_EntityTopic PluginSmartTags_ModuleTags_RegistryCurrentTopic()
 * @method ModuleTopic_EntityTopic[] PluginSmartTags_ModuleTags_GetSimilarTopics(ModuleTopic_EntityTopic $oTopic)
 * @method void Viewer_Assign(string $mName, $mValue = null, bool $bLocal = false, bool $bByRef = false)
 *
 * @package smart_tags.blocks
 * @since 1.0.0
 * @author Eduard Minasyan <mrEDitor@mail.ru>
 */
class PluginSmartTags_BlockSimilarTopics extends Block
{

    /**
     * Inflate similar tags block.
     *
     * @return bool
     */
    public function Exec(): bool
    {
        /** @var ModuleTopic_EntityTopic $oTopic */
        $oTopic = $this->PluginSmartTags_ModuleTags_RegistryCurrentTopic();
        if ($oTopic && $aTopics = $this->PluginSmartTags_ModuleTags_GetSimilarTopics($oTopic)) {
            $sSimilarTopics = $this->_getSimilarTopicsHtml($aTopics);
            $this->Viewer_Assign('similar_topics_content', $sSimilarTopics, true);
            $this->SetTemplate('component@smart_tags:smart-tags.similar-topics');
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get similar topics html.
     *
     * @param ModuleTopic_EntityTopic[] $aTopics
     * @return string
     */
    protected function _getSimilarTopicsHtml(array $aTopics): string
    {
        $oViewer = $this->Viewer_GetLocalViewer();
        $oViewer->Assign('topics', $aTopics, true);
        $sTextResult = $oViewer->Fetch("component@activity.recent-topics");
        return $sTextResult;
    }

}
