<?php
/**
 * Copyright 2018, Eduard Minasyan <mrEDitor@mail.ru>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Smart Tags hooks processor.
 *
 * @method string Lang_Get(string $sKey, array $aReplace = [], bool $bDelete = true)
 * @method void Message_AddError(string $sMsg, string $sTitle = null, bool $bUseSession = false)
 * @method string[]|null PluginSmartTags_ModuleTags_GetTagsFromText(string $text)
 * @method void PluginSmartTags_ModuleTags_RegisterCurrentTopic(ModuleTopic_EntityTopic $oTopic)
 * @method void PluginSmartTags_ModuleTags_SaveTopicTags(int $iTopicId, string[] $aTags)
 *
 * @package smart_tags.hooks
 * @since 1.0.0
 * @author Eduard Minasyan <mrEDitor@mail.ru>
 */
class PluginSmartTags_HookMain extends Hook
{
    /**
     * Register available hooks.
     *
     * @return void
     */
    public function RegisterHook()
    {
        $this->AddHook('topic_edit_after', 'HookTopicEditAfter');
        $this->AddHook('topic_show', 'HookTopicShow');
    }

    /**
     * Edit topic after hook.
     *
     * @param array $aParams
     * @return void
     */
    public function HookTopicEditAfter(array $aParams)
    {
        /** @var ModuleTopic_EntityTopic $oTopic */
        $oTopic = $aParams['oTopic'];
        $aTags = $this->PluginSmartTags_ModuleTags_GetTagsFromText($oTopic->getText());
        if ($aTags !== null) {
            $aTags = array_unique(array_merge($oTopic->getTagsArray(), $aTags));
            $this->PluginSmartTags_ModuleTags_SaveTopicTags($oTopic->getId(), $aTags);
        } else {
            $this->Message_AddError($this->Lang_Get('plugin.smart_tags.tags-extractor.unavailable'));
        }
    }

    /**
     * Show topic hook.
     *
     * @param array $aParams
     * @return void
     */
    public function HookTopicShow(array $aParams)
    {
        /** @var ModuleTopic_EntityTopic $oTopic */
        $oTopic = $aParams['oTopic'];
        $this->PluginSmartTags_ModuleTags_RegisterCurrentTopic($oTopic);
    }

}
