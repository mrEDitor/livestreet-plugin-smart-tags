<?php
/**
 * Copyright 2017, Eduard Minasyan <mrEDitor@mail.ru>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Smart Tags plugin main class.
 *
 * @method bool Viewer_AppendScript(string $sJs, array $aParams = [], bool $bReplace = false)
 * @method bool Viewer_AppendStyle(string $sCss, array $aParams = [], bool $bReplace = false)
 *
 * @package smart_tags
 * @since 1.0.0
 * @author Eduard Minasyan <mrEDitor@mail.ru>
 */
class PluginSmartTags extends Plugin
{

    /**
     * Object declarations.
     *
     * @var array
     */
    protected $aInherits = [
        'action' => ['_ActionSmartTags'],
        'block'  => ['_BlockSimilarTopics'],
        'event'  => [
            '_ModuleSmartTags_EventFetch',
            '_ModuleSmartTags_EventProcess',
        ],
    ];

    /**
     * Try to activate the plugin.
     *
     * @return bool Whether plugin successfully activated.
     */
    public function Activate(): bool
    {
        parent::Activate();

        return true;
    }

    /**
     * Try to deactivate the plugin.
     *
     * @return bool Whether plugin successfully deactivated.
     */
    public function Deactivate(): bool
    {
        parent::Deactivate();

        return true;
    }

    /**
     * Try to remove the plugin.
     *
     * @return bool Whether plugin successfully removed.
     */
    public function Remove(): bool
    {
        parent::Remove();

        return true;
    }

    /**
     * Initialize the plugin.
     *
     * @return void
     */
    public function Init(): void
    {
        parent::Init();

        $sAssetsPath = Plugin::GetTemplateWebPath(__CLASS__) . 'assets/';

        $this->Viewer_AppendStyle($sAssetsPath . 'css/main.css');

        $this->Viewer_AppendScript($sAssetsPath . 'js/main.js');
        $this->Viewer_AppendScript($sAssetsPath . 'js/tags.js');
        $this->Viewer_AppendScript($sAssetsPath . 'js/text.js');

    }

}
