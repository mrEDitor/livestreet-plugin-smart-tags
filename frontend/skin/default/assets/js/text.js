/**
 * Copyright 2017, Eduard Minasyan <mrEDitor@mail.ru>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Text field UI and logic.
 *
 * @param tags {@see tags.js}
 *
 * @package smart_tags
 * @since 1.0.0
 * @author Eduard Minasyan <mrEDitor@mail.ru>
 */
window.PluginSmartTags_text = function (tags) {
    this.INPUT_NAME = 'topic[topic_text_source]';
    this.PROCESSING_URL = aRouter['smart_tags'] + 'process';
    this.PROCESSING_INTERVAL = 5 * 60 * 1000;
    this.input = $('[name="' + this.INPUT_NAME + '"]');
    this.tags = tags;

    /**
     * If processing scheduled in {@see PROCESSING_INTERVAL} interval,
     * stores returned by {@see setInterval} handler.
     *
     * @type {number}
     */
    this.scheduledProcessing = false;

    /**
     * Pretty simple hash function of string.
     *
     * @param str   String to hash.
     * @return int
     */
    this.hashString = function (str) {
        var hash = 5381, i = str.length;
        while (i) {
            hash = (hash * 33) ^ str.charCodeAt(--i);
        }
        return hash >>> 0;
    };

    /**
     * Get plain text as simplified sentences.
     *
     * @return string
     */
    this.getPlainText = function () {
        return $.trim(this.input.val())
            .replace(/<code>.*<\/code>/ug, '')
            .replace(/<\/?[^>]+>/ug, '')
            .replace(/[!?]/ug, '.')
    };

    /**
     * Send topic text to server for processing and fetch result.
     *
     * @return void
     */
    this.process = function () {
        var text = this.getPlainText();
        if (!this.tags.restore(this.hashString(text))) {
            $.ajax({
                context: this,
                url: this.PROCESSING_URL,
                method: 'POST',
                data: {text: text},
                dataType: 'json',
                success: this.onProcessResponse
            });
        }
    };

    /**
     * Apply processed tags from server.
     *
     * @param data.status       Whether success or error.
     * @param data.suggestions  Suggestions, if any.
     * @param data.message      Error message, if any.
     * @return void
     */
    this.onProcessResponse = function (data) {
        if (data.status === 'success') {
            var text = this.getPlainText();
            this.tags.suggest(this.hashString(text), data.suggestions);
        } else {
            console.log(data.message);
        }
    };

    /**
     * Schedule topic text processing.
     *
     * @return void
     */
    this.scheduleProcessing = function () {
        this.scheduledProcessing = setInterval($.proxy(this, 'process'), this.PROCESSING_INTERVAL);
    };

    /**
     * Fire scheduled topic text processing right now.
     *
     * @return void
     */
    this.triggerProcessing = function () {
        clearInterval(this.scheduledProcessing);
        this.scheduledProcessing = false;
        this.process();
    };
};
