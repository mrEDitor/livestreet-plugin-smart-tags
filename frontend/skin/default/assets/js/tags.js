/**
 * Copyright 2017, Eduard Minasyan <mrEDitor@mail.ru>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Tags field UI and logic.
 *
 * @package smart_tags
 * @since 1.0.0
 * @author Eduard Minasyan <mrEDitor@mail.ru>
 */
window.PluginSmartTags_tags = function () {
    this.INPUT_NAME = 'topic[topic_tags]';
    this.SUGGESTIONS_CLASS = 'smart-tags-suggest';
    this.SEPARATOR_TEXT = ', ';
    this.SLIDE_DURATION = 300;
    this.input = $('[name="' + this.INPUT_NAME + '"]');
    this.suggestions = $('<div>', {class: this.SUGGESTIONS_CLASS});

    /**
     * Cache tag suggestion arrays by text key.
     *
     * @type object
     */
    this.cache = {};

    /**
     * Escape string to use as regexp.
     *
     * @param str   String to escape.
     * @return string
     */
    this.escapeRegExp = function (str) {
        return str.replace(/[-{}()*+?.^$|[\/\\\]]/g, '\\$&');
    };

    /**
     * Check are two arrays identical, not considering element order.
     *
     * @param a First array to compare.
     * @param b Second array to compare.
     * @return boolean
     */
    this.areArraysEqual = function (a, b) {
        a = a.slice().sort();
        b = b.slice().sort();
        if (a.length !== b.length) return false;
        return a.every(function (v, i) {
            return v === b[i];
        });
    };

    /**
     * Add tag to input field.
     *
     * @param e.target  Text node to add.
     * @return void
     */
    this.add = function (e) {
        var oldValues = this.input.val().replace(/,? *$/g, '');
        var newValue = $(e.target).text();
        var isTagExist = new RegExp('(?:^|(?: ?, ?))' + this.escapeRegExp(newValue) + '(?:(?: ?, ?)|(?: ?$))', 'gi');
        if (!isTagExist.test(oldValues.replace(/ +/g, ' '))) {
            this.input.val(oldValues + (oldValues ? this.SEPARATOR_TEXT : '') + newValue + this.SEPARATOR_TEXT);
        }
        this.input.focus();
    };

    /**
     * Restore previously processed suggestions.
     *
     * @param cacheKey  Text cache key to restore.
     * @return boolean Whether suggestions restored.
     */
    this.restore = function (cacheKey) {
        if (cacheKey in this.cache) {
            this.suggest(cacheKey, this.cache[cacheKey]);
            return true;
        } else {
            return false;
        }
    };

    /**
     * Add tag to suggestions.
     *
     * @param cacheKey      Text cache key to associate.
     * @param suggestions   Array of suggestions as text nodes.
     * @return void
     */
    this.suggest = function (cacheKey, suggestions) {
        var suggestedBefore = $.map(this.suggestions.children('a'), $.text);
        if (this.areArraysEqual(suggestions, suggestedBefore)) return;

        this.suggestions.slideUp(this.SLIDE_DURATION, $.proxy(function () {
            this.suggestions.empty();
            $(suggestions)
                .map($.proxy(this, 'toSuggestion'))
                .appendTo(this.suggestions);
            this.suggestions
                .slideDown(this.SLIDE_DURATION);
        }, this));

        this.cache[cacheKey] = suggestions;
    };

    /**
     * Text-to-link mapping callback.
     *
     * @param index In-array key.
     * @param str   Suggestion string value.
     * @return Node
     */
    this.toSuggestion = function (index, str) {
        return $('<a>')
            .text(str)
            .click($.proxy(this, 'add'))
            .get(0);
    };
};
