<?php
/**
 * Copyright 2017, Eduard Minasyan <mrEDitor@mail.ru>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Since this file is under VCS index, {@see config.local.php.dist} for changing values.
 */
return [
    '$root$' => [
        'block' => [
            'rule_index_blog' => [
                'blocks' => [
                    'right' => [
                        'similarTopics' => [
                            'params' => ['plugin' => 'smart_tags'],
                            'priority' => 150,
                        ],
                    ],
                ],
            ],
        ],
        'db' => [
            'table' => [
                'smart_tags' => '___db.table.prefix___' . 'smart_tags',
            ],
        ],
        'router' => [
            'page' => [
                'smart_tags' => 'PluginSmartTags_ActionSmartTags',
            ],
        ],
    ],
    'suggestions' => ['limit' => 7],
    'similar_topics' => ['limit' => 5],
    'extractor_url' => 'http://localhost/cgi-bin/extract_keywords',
];
