# Smart Tags
## LiveStreet CMS Plugin

### Features

- Add tags suggestions to topic edit page.
- Add Similar Topics block to right column of topic view page.

### Requirements

- LiveStreet CMS and all it's requirements, plus:
    - PHP 7.1+
    - MySQL 5.6+
- RESTful keyword extraction service:
    - https://gitlab.com/mrEDitor/rake-nltk-rest/ or compatible.

### Installation

1. Get plugin distributable files. Most recent one you can get directly from
    project git repository on
    https://gitlab.com/mrEDitor/livestreet-plugin-smart-tags/
2. The plugin should be installed just like any regular LiveStreet CMS plugin,
    simply copy obtained files to `application/plugins/smart_tags` directory
    of yours LiveStreet CMS installation.
3. Update module configuration files in `application/plugins/smart_tags/config`
    directory. Pay attention to `extractor_url` option.
4. Enable **Smart Tags** plugin in LiveStreet CMS Admin Panel.

### License

Smart Tags plugin in licensed under Apache License 2.0. See LICENSE file
for details.
